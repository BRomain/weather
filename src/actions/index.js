import constants from './../constants';

export function fetchWeather(request) {
    return {
        type: constants.FETCH_WEATHER,
        payload: request
    }
}