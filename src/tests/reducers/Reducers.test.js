import reducers from './../../reducers'

const generateFakeCity = () => {
    return {
        data: {
            cod: "200",
            message: 0.0071,
            cnt: 40,
            list: [
                {
                    dt: 1555599600,
                    main: {
                        temp: 21.53,
                        temp_min: 19.72,
                        temp_max: 21.53,
                        pressure: 1017.484,
                        sea_level: 1017.484,
                        grnd_level: 960.64,
                        humidity: 42,
                        temp_kf: 1.81
                    },
                    weather: {},
                    clouds: {},
                    wind: {},
                    sys: {},
                    dt_txt: "2019-04-18 15:00:00"
                }
            ],
            city: {
                id: 2996944,
                name: "Lyon",
                country: "FR",
                population: 472317,
                coord: {
                    lat: 45.7578,
                    lon: 4.832
                }
            }
        }
    }
}

describe("Reducers", () => {
    test("State vide", () => {
        // Action
        const action = {
            type: null,
        }

        const weather = reducers([], action)
        expect(weather).toEqual([])
    })
    test("Ajout d'une ville", () => {
        const ville = generateFakeCity()

        // Action
        const action = {
            type: 'FETCH_WEATHER',
            payload: ville
        }

        const weather = reducers([], action)
        expect(weather).toEqual([ville.data])
    })
})
