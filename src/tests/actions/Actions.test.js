import React from 'react';
import { fetchWeather } from './../../actions'

describe("Actions", () => {
    test("fetchWeather", () => {
        const expectedAddTodo = {
            type: 'FETCH_WEATHER',
            payload: {}
        }
        expect(fetchWeather({})).toEqual(expectedAddTodo)
    })
})
