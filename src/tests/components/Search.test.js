import React from 'react';
import Enzyme, {shallow} from 'enzyme';
import Search from './../../components/Search'
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import sinon from 'sinon'
import mockAxios from 'jest-mock-axios';

const mockStore = configureMockStore();
const store = mockStore({});

Enzyme.configure({ adapter: new EnzymeAdapter() });

afterEach(() => {
    mockAxios.reset();
});

describe("Composant - Search", () => {
    test("Ne crash pas", () => {
        const props = {
            fetchWeather: jest.fn()
        }
        const wrapper = shallow(
            <Provider store={store}>
                <Search/>
            </Provider>
        )

        expect(wrapper.exists()).toBe(true);
    })

    test("Le state city correspond bien à un input", () => {
        const wrapper = shallow(
            <Search store={store}/>
        ).dive()

        expect(wrapper.state('city')).toEqual(React.createRef())
    })

    test("Le formulaire s'envoie bien", () => {
        const wrapper = shallow(
            <Search store={store}/>
        ).dive()

        wrapper.instance().inputValue = sinon.stub().returns("Lyon")
        wrapper.update()

        wrapper.find('form').simulate('submit', {
            preventDefault: () => {
                prevented: true
            }
        })
    })

    test("L'input est valide", () => {
        const wrapper = shallow(
            <Search store={store}/>
        ).dive()

        wrapper.instance().inputValue = sinon.stub().returns("Lyon")

        expect(wrapper.instance().checkInputValidity()).toEqual(true)
    })

    test("L'input est valide", () => {
        const wrapper = shallow(
            <Search store={store}/>
        ).dive()

        wrapper.instance().inputValue = sinon.stub().returns(null)

        expect(wrapper.instance().checkInputValidity()).toEqual(false)
    })
})
