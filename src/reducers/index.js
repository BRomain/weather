import constants from './../constants';

export default function (state = [], action) {
    switch (action.type) {
        case constants.FETCH_WEATHER:
            return [ action.payload.data, ...state ]
        default:
            return state;
    }
}