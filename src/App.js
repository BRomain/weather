import React, { Component } from 'react';
import './App.scss';

// Components
import Search from './components/Search';
import ListCitysContainer from './containers/ListCitysContainer';

class App extends Component {
  render() {
    return (
      <div>
        <Search/>
        <ListCitysContainer/>
      </div>
    );
  }
}

export default App;
