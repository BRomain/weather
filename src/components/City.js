import React, { Component } from 'react'
import Temp from './Temp'

class City extends Component {
    render() {
        return (
            <div className="weather-city">
                <h1>{ this.props.name }</h1>
                <div className="weather-element">
                    { this.props.list.map((temp, i) => {
                        return (
                            <Temp date={temp.dt_txt} temp={temp.main} key={i}/>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default City

