import React, { Component } from 'react'
import { fetchWeather } from './../actions'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import axios from 'axios';

const API_KEY = '7b9d891e4746d2189cda9806a70b9b26';
const URL = `https://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}&units=metric`;

class Search extends Component {

    /**
     * Constructeur de la classe qui instancie le constructuer de la classe parente
     * 
     * @param {Object} props - Props de la classe
     */
    constructor(props) {
        super(props)

        this.state = {
            city: React.createRef(),
            err: false
        }
    }

    /**
     * Vérifie la validité de l'input, si il n'y a rien dedans, il renvoie false
     * 
     * @return {Boolean}
     */
    checkInputValidity() {
        return  this.inputValue() ? true : false
    }

    /**
     * Soumet le formulaire et execute notre requête
     * 
     * @param {Object} e - L'evenement du submit
     */
    onSubmit(e) {
        e.preventDefault()
        if (this.checkInputValidity()) {
            const url = `${URL}&q=${this.inputValue()}`;
            const request = axios.get(url);

            request
                .then(r => {
                    this.props.fetchWeather(r)
                    this.setState({city: React.createRef()})
                })
                .catch(e => this.setState({err : e.response.data.message}))
        }
    }

    /**
     * Retourne la valeur de l'input courant
     * 
     * @return {String}
     */
    inputValue() {
        return this.state.city.current.value
    }

    /**
     * Fonction de retour
     */
    render() {
        return (
            <form
                onSubmit={e => this.onSubmit(e)}
                className="weather-form"
            >
                { this.state.err }
                <input ref={this.state.city} type="text"/>
                <button type="submit">Voir</button>
            </form>
        )
    }
} 

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchWeather }, dispatch)
}
  
export default connect(null, mapDispatchToProps)(Search)