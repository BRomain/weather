import React from 'react'
import City from './../components/City'

const ListCitys = ({ citys }) => (
    <div>
        { citys.map((city, i) => {
            return (
                <City name={city.city.name} list={city.list} key={i}/>
            )
        })}
    </div>
)

export default ListCitys

