import React, { Component } from 'react'

class Temp extends Component {
    render() {
        return (
            <div className="weather-temp">
                <p>Jour : { this.props.date }</p>
                <div className="weather-temp_temp">
                    <p>Temp : { this.props.temp.temp } °C</p>
                    <p>Temp Min. : { this.props.temp.temp_min } °C</p>
                    <p>Temp Max. : { this.props.temp.temp_max } °C</p>
                </div>
            </div>
        )
    }
}

export default Temp

