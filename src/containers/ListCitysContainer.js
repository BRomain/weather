import ListCitys from './../components/ListCitys'
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return {
        citys: state
    }
};

export default connect(
    mapStateToProps
)(ListCitys)
  