### Application Météo

BLANCHET Romain - DFS 11

## Tour d'horizon

L'application est réalisé avec le Framework JS [React](https://reactjs.org/). Elle utilise un gestionnaire d'etat [Redux](https://redux.js.org/) qui nous permet d'avoir une meilleure arborescence.

## Doc

* [Git](docs/git.md)
* [Deploiements](docs/deploy.md)
* [Tests](docs/tests.md)
