## Tests unitaire

Nous utilisons dans le projet deux outils afin de rendre le code plus uniforme et mieux testable

* __[ESLint](https://eslint.org/)__ : Cet outil permet de formaliser le code, nous utilisons la convetion d'AirBnB
* __[Jest](https://jestjs.io/)__ : C'est un outil de test unitaire, vous développerez vos tests dans le dossier `/tests` prévu à cet effet, essayez de respectez l'arborescence qu'on pourrait trouver dans le dossier `/src`