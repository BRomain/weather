## Deploiement de l'application

L'application intègre un outil qui permet de mettre automatiquement l'application sur notre FTP dès le commit sur le master.

Il faut qu'au préalable vos tests soit passés et que la Pull Request validée, ces deux points remplis un de nos Mainteners pourra merger sur le master, l'outil prendra le relai pour mettre en production automatiquement.