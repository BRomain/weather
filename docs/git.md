## Git

Afin de conserver une bonne gestion nous utiliserons la convetion [Git Flow](https://nvie.com/posts/a-successful-git-branching-model/)

Nous nommerons nos branches en fonction des besoin du ticket :
* __feat__ : Concerne tous les tickets qui concerne un nouvel ajout sur l'application
* __fix__ : Concerne un fix non urgent
* __hotfix__ : Concerne un fix urgent, en general il est déployer directement sur la recette/prod
* __refacto__ : Concerne une refactorisation du code

Chaque nouveaux developpements devront faire l'objet d'une nouvelle branche avec deux PR, une sur la master, l'autre sur la Recette, le commit sur Master n'etant pas autoriser, sa ne sert à rien de contrer ces méthodes !